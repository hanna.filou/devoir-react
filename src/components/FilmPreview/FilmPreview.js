import React from 'react';
import { Link } from 'react-router-dom';
class FilmPreview extends React.Component {
    render() {
        const { slug, title, releaseYear, description} = this.props;
        
        return (
            <article>
                
                <p>Titre : { title }</p>
                <p>Date de réalisation : { releaseYear }</p>
                <Link to={'/film/' + slug} >Read</Link>
                
               
            </article>
            
        );
    }
}

export default FilmPreview;