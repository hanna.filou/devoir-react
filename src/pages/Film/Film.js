import React from 'react';
import { getOneFilm } from '../../api.js';
import FilmList from '../../components/FilmList/FilmList.js';

export default class Film extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        slug: '',
        title: '',
        releaseYear: '',
        description: '',
        similarFilms: [],
        isNotFound: false
      };

    }
    componentDidMount() {
        const { match } = this.props;
        
        const slug = match.params.slug;
        this.setFilm(slug);
      }

      setFilm(slug){
        const film = getOneFilm(slug);
        
        if (film.similarFilms === null) {
            this.setState({ isNotFound: true });
          }else {
            this.setState({
              slug: film.slug,
              title: film.title,
              releaseYear: film.releaseYear,
              description: film.description,
              similarFilms: film.similarFilms
            });
          } 
      }
      componentDidUpdate(prevProps) {
       
        if (prevProps.match.params.slug !== this.props.match.params.slug) {
            const slug = this.props.match.params.slug;
            this.setFilm(slug);
            
        }
      }
      render() {
        const { title, releaseYear, description, isNotFound, similarFilms } = this.state;
    
        if (isNotFound === true) {
          return (<h1>Film not found....</h1>);
        }
    
        return (
          <section>
            <h1>{ title }</h1>
            <p>at { releaseYear }</p>
            <p>{ description }</p>
            <div>
            <h4>Films Similiaires</h4>
            <div>

            {similarFilms.length  !== 0 ? 
                    (<FilmList films={similarFilms}/>)
                    :
                    (<p>Aucun film similiaire n'a été trouvé !</p>)
            }
            </div>
              
            </div>
          </section>
        );
      }

    }