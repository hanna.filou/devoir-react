import React from 'react';
import { getAllFilms } from '../../api.js';
import FilmList from '../../components/FilmList/FilmList.js';
export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      films: [],
      isLoading: false
    };
  }
  
  componentDidMount() {
    this.setState({ isLoading: true });

    const films = getAllFilms();
    this.setState({
     films: films
    });
  }
  
  render() {
    const {films} =this.state;
    return (
      <div>
        <h1>Tous nos films</h1>
        
          <FilmList films={films}/>
        
      </div>
    );
  }
}
