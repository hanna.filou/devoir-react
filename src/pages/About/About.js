import React from 'react';
class About extends React.Component {
    render() {
        return (
            <section>
                <h1>Strictly Criminal</h1>
                <p>Le quartier de South Boston dans les années 70. L'agent du FBI John Connolly convainc le caïd irlandais James "Whitey" Bulger de collaborer avec l'agence fédérale afin d'éliminer un ennemi commun : la mafia italienne. Le film retrace l'histoire vraie de cette alliance contre nature qui a dégénéré et permis à Whitey d'échapper à la justice, de consolider son pouvoir et de s'imposer comme l'un des malfrats les plus redoutables de Boston et les plus puissants des États-Unis.</p>
            </section>
        );
    }
}
export default About;